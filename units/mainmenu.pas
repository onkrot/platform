unit MainMenu;

{$mode objfpc}{$H+}

interface

uses
  MondoZenGL,Button;

type

  { TMainMenuScene }

  TMainMenuScene = class(TMZScene)
  private
    FFont:TMZFont;
    FbtnNew:TButton;
  protected
    procedure Startup; override;
    procedure Shutdown; override;
    procedure RenderFrame; override;
    procedure Update(const DeltaTimeMs: Double); override;
    procedure NewGameClick(Sender:TObject);
  end;

implementation
uses
  Main;

procedure TMainMenuScene.NewGameClick(Sender:TObject);
begin
  NewGame();
end;

{ TMainMenuScene }

procedure TMainMenuScene.Startup;
begin
  inherited Startup;
  FFont:=TMZFont.Create('data/font.zfi');
  FbtnNew:=TButton.Create(Canvas,FFont,'New Game',100,100,@NewGameClick);
end;

procedure TMainMenuScene.Shutdown;
begin
  FbtnNew.Free;
  FFont.free;
  inherited Shutdown;
end;

procedure TMainMenuScene.RenderFrame;
begin
  FbtnNew.Render;
end;

procedure TMainMenuScene.Update(const DeltaTimeMs: Double);
begin
  FbtnNew.IsClicked;
end;

end.

