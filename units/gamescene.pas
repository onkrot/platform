unit GameScene;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,MondoZenGL,mzTileMaps,mzChipmunk,PhysicalObject, Contnrs;

type

  { THero }

  THero = class(TObject)
  private
    FSprite:TMZSprite;
    FTexture:TMZTexture;
    FPhysicalObject: TPhysicalObject;
  public
    property PhysicalObject: TPhysicalObject read FPhysicalObject write FPhysicalObject;
    constructor Create(FileName:UTF8String;SpriteEngine:TMZSpriteEngine;X,Y:Single);
    destructor Free;
    procedure Update;
  end;

  { TGameScene }

  TGameScene = class(TMZScene)
  private
    FFont: TMZFont;
    FMap: TMZTileMap;
    FSpriteEngine: TMZSpriteEngine;
    FSpace: TCPSpace;
    FHero: THero;
    FMapObjects: TObjectList;
  protected
    procedure Startup; override;
    procedure Shutdown; override;
    procedure RenderFrame; override;
    procedure Update(const DeltaTimeMs: Double); override;
    procedure KeyDown(const KeyCode: TMZKeyCode);
    procedure KeyUp(const KeyCode: TMZKeyCode);
    property Space:TCPSpace read FSpace;
    property SpriteEngine:TMZSpriteEngine read FSpriteEngine;
  end;

implementation
uses
  Main;

{ THero }

constructor THero.Create(FileName: UTF8String; SpriteEngine: TMZSpriteEngine;
  X, Y: Single);
const
  WIDTH = 32;
  HEIGHT = 32;
begin
  inherited Create;
  FTexture:=TMZTexture.Create(FileName);
  FSprite:=TMZSprite.Create(SpriteEngine, FTexture);
  FSprite.X:=X;
  FSprite.Y:=Y;
  FPhysicalObject:=TPhysicalObject.Create(Game.Space,x,y,width,height)
end;

destructor THero.Free;
begin
  FSprite.Free;
  FTexture.Free;
  FPhysicalObject.Free;
  inherited Free;
end;

procedure THero.Update;
begin
  FSprite.Position := FPhysicalObject.Position;
end;

{ TGameScene }

procedure TGameScene.Startup;
var v,m,p:integer;
  tmp:TPhysicalObject;
begin
  inherited Startup;
  FFont:=TMZFont.Create('data/font.zfi');
  FMap:=TMZTileMap.Create('data/map.mztm');
  FMap.ViewCenterX:=500;
  FMap.ViewCenterY:=300;
  FSpriteEngine:=TMZSpriteEngine.Create;
  p:=0;
  FSpace:=TCPSpace.Create;
  FSpace.Iterations := 10;
  FSpace.ElasticIterations := 10;
  FSpace.Gravity:=CPV(0, 25);
  Fhero:=THero.Create('data/hero.png',SpriteEngine,128,128);
  for v:=0 to Fmap.Layers[1].ColumnCount - 1 do
    for m:= 0 to Fmap.Layers[1].RowCount -1  do
    begin
      if Fmap.Layers[1].Cells[v,m] > 0 then
      begin
        tmp:=TPhysicalObject.CreateStatic(Space,v*32,m*32,32,32);
        //FMapObjects.Add(TObject(tmp));
      end;
    end;
  write(p);
end;

procedure TGameScene.Shutdown;
begin
  FFont.Free;
  FMap.Free;
  inherited Shutdown;
end;

procedure TGameScene.RenderFrame;
begin
  FMap.Render;
  FSpriteEngine.Sprites[0].Draw;
  inherited RenderFrame;
end;

procedure TGameScene.Update(const DeltaTimeMs: Double);
begin
  inherited Update(DeltaTimeMs/1000);
  FSpace.Step(DeltaTimeMs/1000);
  FHero.Update;
end;

procedure TGameScene.KeyDown(const KeyCode: TMZKeyCode);
begin
  case KeyCode of
    kcLeft:
  end;
end;

procedure TGameScene.KeyUp(const KeyCode: TMZKeyCode);
begin

end;

end.

