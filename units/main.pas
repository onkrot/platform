unit Main;

{$mode objfpc}{$H+}

interface

uses
  MondoZenGL,MainMenu,GameScene;

var
  App:TMZApplication;
  Game:TGameScene;
  MainMenuScene:TMainMenuScene;

procedure Run();
procedure NewGame();

implementation

procedure Run;
begin
  App:=TMZApplication.Create;
  App.Options := App.Options
    + [aoShowCursor, aoUseSound, aoUseInputEvents,aoEnableLogging];
  App.Caption:='Game';
  App.ScreenHeight:=600;
  App.ScreenWidth:=1000;
  App.SetScene(TMainMenuScene.Create);
end;

procedure NewGame;
begin
  Game:=TGameScene.Create;
  App.SetScene(Game);
end;

end.

