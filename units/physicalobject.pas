unit PhysicalObject;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, MondoZenGL, mzChipmunk;

type

  { TPhysicalObject }

  TPhysicalObject = class(TObject)
  private
    FShape:TCPPolyShape;
    FBody:TCPBody;
    function GetPosition: TMZPoint;
  public
    property Shape:TCPPolyShape read FShape write FShape;
    property Body:TCPBody read FBody write FBody;
    property Position:TMZPoint read GetPosition;
    constructor Create(Space:TCPSpace;x,y:Single;width,height:TCPFloat);
    constructor CreateStatic(Space:TCPSpace;x,y:Single;width,height:TCPFloat);
    destructor Free();
  end;

implementation

{ TPhysicalObject }

function TPhysicalObject.GetPosition: TMZPoint;
begin
  Result:= TMZPoint.Create(FShape.TV[0].X,FShape.TV[0].Y);
end;

constructor TPhysicalObject.Create(Space: TCPSpace; x, y:Single;width, height:
  TCPFloat);
const
  MASS = 1;
  ELASTICITY = 0;
  FRICTION = 0;
var
  Moment: TCPFloat;
  verts: array [0..3] of TCPVect;
begin
  inherited Create();
  verts[0].X := -WIDTH / 2;
  verts[0].Y := -HEIGHT / 2;
  verts[1].X := -WIDTH / 2;
  verts[1].Y := HEIGHT / 2;
  verts[2].X := WIDTH / 2;
  verts[2].Y := HEIGHT / 2;
  verts[3].X := WIDTH / 2;
  verts[3].Y := -HEIGHT / 2;
  Moment := TCPBody.MomentForPolygon(MASS, VERTS, CPVZero);
  FBody := TCPBody.Create(MASS, Moment);
  FBody.Position := CPV(x + (WIDTH / 2), y + (HEIGHT / 2));
  FShape:=TCPPolyShape.Create(FBody, VERTS, CPVZero);
  FShape.Elasticity := ELASTICITY;
  FShape.Friction := FRICTION;
  Space.AddBody(Self.Body);
  Space.AddShape(Self.Shape);
end;

constructor TPhysicalObject.CreateStatic(Space: TCPSpace; x, y: Single; width,
  height: TCPFloat);
const
  ELASTICITY = 0;
  FRICTION = 0;
var
  verts: array [0..3] of TCPVect;
begin
  inherited Create();
  verts[0].X := -WIDTH / 2;
  verts[0].Y := -HEIGHT / 2;
  verts[1].X := -WIDTH / 2;
  verts[1].Y := HEIGHT / 2;
  verts[2].X := WIDTH / 2;
  verts[2].Y := HEIGHT / 2;
  verts[3].X := WIDTH / 2;
  verts[3].Y := -HEIGHT / 2;
  FBody := TCPBody.Create(INFINITY, INFINITY);
  FBody.Position := CPV(x + (WIDTH / 2), y + (HEIGHT / 2));
  FShape:=TCPPolyShape.Create(FBody, VERTS, CPVZero);
  FShape.Elasticity := ELASTICITY;
  FShape.Friction := FRICTION;
  Space.AddBody(Self.Body);
  Space.AddStaticShape(Self.Shape);
end;

destructor TPhysicalObject.Free;
begin
  FBody.Free;
  FShape.Free;
  inherited Free;
end;

end.

