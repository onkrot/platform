unit Button;

{$mode objfpc}{$H+}

interface

uses
  SysUtils,MondoZenGL,Classes;

type
  TButton = class
  private
    FCanvas: TMZCanvas;
    FFont: TMZFont;
    FCaption: UTF8String;
    FBounds: TMZRect;
    FOnClick:TNotifyEvent;
    FMousePosition:TMZPoint;
  public
    constructor Create(const Canvas: TMZCanvas; const Font: TMZFont;
      const Caption: UTF8String; const X, Y: Single; const EventClick:TNotifyEvent);
    procedure Render;
    function IsClicked: Boolean;
    property OnClick:TNotifyEvent read FOnClick;
    property Bounds: TMZRect read FBounds;
  end;

implementation

{ TButton }

constructor TButton.Create(const Canvas: TMZCanvas; const Font: TMZFont;
  const Caption: UTF8String; const X, Y: Single; const EventClick:TNotifyEvent);
begin
  inherited Create;
  FCanvas := Canvas;
  FFont := Font;
  FCaption := Caption;
  FBounds.X := X;
  FBounds.Y := Y;
  FBounds.W := FCanvas.CalculateTextWidth(FFont, FCaption) + 8;
  FBounds.H := FCanvas.CalculateTextHeight(FFont, FBounds.W, FCaption) + 8;
  FOnClick:=EventClick;
  FMousePosition:=TMZPoint.Create(0,0);
end;

procedure TButton.Render;
begin
  FCanvas.FillRect(FBounds, $15428B);
  FCanvas.DrawText(FFont, FBounds, FCaption, [tfHAlignCenter, tfVAlignCenter]);
end;

function TButton.IsClicked: Boolean;
begin
  FMousePosition.X:=TMZMouse.X;
  FMousePosition.Y:=TMZMouse.Y;
  Result:=(TMZMouse.IsButtonDown(mbLeft)) and (FBounds.ContainsPoint(FMousePosition));
  if Result then
     FOnClick(Self);
end;

end.

